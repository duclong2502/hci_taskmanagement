@extends('master.app')
@section('title','Thêm nhân viên')
@section('content')
        <div class="my-3 my-md-5">
            <div class="container">
              <div class="page-header d-flex justify-content-center">
                <h1 class="page-title">Thêm mới nhân viên</h1>
              </div>
              <div class="row row-cards row-deck d-flex justify-content-center">
                <div class="col-6">
                  <div class="card">
                    <div class="card-status bg-green"></div>
                    <div class="card-header">Thêm</div>
                    <div class="card-body">
                        <form action="{{route('category.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="category_title" class="form-control {{($errors->has('category_title'))?'is-invalid':''}}" value="{{old('category_title')}}" placeholder="Tên nhân viên">
                                @if($errors->has('category_title'))
                                   <p class="text-danger">{{$errors->first('category_title')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" name="name" class="form-control {{($errors->has('category_title'))?'is-invalid':''}}" value="{{old('category_title')}}" placeholder="Chức vụ">
                                @if($errors->has('category_title'))
                                    <p class="text-danger">{{$errors->first('category_title')}}</p>
                                @endif
                            </div>
                            <div class="card-footer float-right">
                                <a href="{{route('category.index')}}" class="btn btn-danger">Hủy</a>
                                <button type="submit" class="btn btn-success">Lưu</button>
                            </div>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    @endsection
