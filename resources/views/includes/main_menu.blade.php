
<div class="row align-items-center">
    <div class="col-lg order-lg-first">
        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
        <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {{Request::is('/')?'active':''}}"><i class="fe fe-home"></i>Phòng giám sát</a>
        </li>
            <li class="nav-item dropdown">
                <a href="javascript:void(0)" class="nav-link {{Request::is('projects/*')?'active':''}}" data-toggle="dropdown"><i class="fe fe-calendar"></i> Dự án</a>
                <div class="dropdown-menu dropdown-menu-arrow">
                    <a href="{{route('project.all')}}" class="dropdown-item {{Request::is('projects/all')?'active':''}}">Dự án</a>
                    <a href="{{route('project.ongoing')}}" class="dropdown-item {{Request::is('projects/ongoing')?'active':''}}">Dự án đang triển khai</a>
                    <a href="{{route('project.finished')}}" class="dropdown-item {{Request::is('projects/finished')?'active':''}}">Dự án hoàn thành</a>
                </div>
            </li>
        <li class="nav-item">
            <a href="{{route('category.index')}}" class="nav-link {{Request::is('categories')?'active':''}}"><i class="fe fe-calendar"></i> Nhân viên</a>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link {{Request::is('tasks/*')?'active':''}}" data-toggle="dropdown"><i class="fe fe-box"></i> Nhiệm vụ</a>
            <div class="dropdown-menu dropdown-menu-arrow">
                <a href="{{route('task.ongoing')}}" class="dropdown-item {{Request::is('tasks/ongoing')?'active':''}}">Nhiệm vụ của tôi</a>
                <a href="{{route('task.pending')}}" class="dropdown-item {{Request::is('tasks/pending')?'active':''}}">Nhiệm vụ chờ xử lý</a>
                <a href="{{route('task.completed')}}" class="dropdown-item {{Request::is('tasks/completed')?'active':''}}">Nhiệm vụ đã hoàn thành</a>
            </div>
        </li>

        <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link {{Request::is('project_tasks/*')?'active':''}}" data-toggle="dropdown"><i class="fe fe-box"></i> Nhiệm vụ dự án</a>
            <div class="dropdown-menu dropdown-menu-arrow">
                <a href="{{route('project_task.all')}}" class="dropdown-item {{Request::is('project_tasks/all')?'active':''}}">Nhiệm vụ dự án</a>
                <a href="{{route('project_task.pending')}}" class="dropdown-item {{Request::is('project_tasks/pending')?'active':''}}">Nhiệm vụ chờ xử lý</a>
                <a href="{{route('project_task.finished')}}" class="dropdown-item {{Request::is('project_tasks/finished')?'active':''}}">Nhiệm vụ dự án đã hoàn thành</a>
            </div>
        </li>
        </ul>
    </div>
</div>
