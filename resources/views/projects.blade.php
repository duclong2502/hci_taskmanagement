@extends('master.app')
@section('title','Ongoing Projects')
@section('content')
        <div class="my-3 my-md-5">
            <div class="container">
              <div class="page-header">
                <h1 class="page-title">Dự án</h1>
                <div class="row gutters-xs ml-auto">
                    <div class="col">
                        <a href="{{route('project.create')}}" class="btn btn-success">Khởi tạo dự án</a>
                    </div>
                </div>
              </div>
              @if(Session::has('message'))
                 <p class="alert alert-success">{{Session::get('message')}}</p>
              @endif
              <div class="row row-cards row-deck">
                <div class="col-12">
                  <div class="card p-4">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped card-table table-vcenter text-nowrap">
                            <thead>
                              <tr>
                                <th>Tên dự án</th>
                                <th>Người phụ trách</th>
                                <th>Trạng thái</th>
                                  <th>Mô tả</th>
                                <th>Khởi tạo</th>
                                <th>Ngày bắt đầu</th>
                                <th>Ngày kết thúc</th>
                                <th>Thao tác</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($projects as $project)
                                <tr>
                                  <td>{{$project->title}}</td>
                                  <td>{{$project->category->category_title}}</td>
                                  <td>
                                      @if($project->status=='pending')
                                        <p class="text-center text-light bg-warning m-2 p-1">Chờ xử lý</p>
                                      @else
                                        <p class="text-center text-light bg-success m-2">Hoàn thành</p>
                                      @endif
                                  </td>
                                    <td>{{$project->description}}</td>
                                  <td>{{date_format(date_create($project->created_at),'d M,Y')}}</td>
                                  <td>{{date_format(date_create($project->start_date),'d M,Y')}}</td>
                                  <td>{{date_format(date_create($project->end_date),'d M,Y')}}</td>
                                  <td>
                                     @if($project->status=='pending')
                                      <a href="" class="btn btn-success" onclick="
                                        if(confirm('Bạn có muốn xóa ?')){
                                          event.preventDefault();
                                          document.getElementById('make_completed-{{$project->id}}').submit();
                                        }else{
                                          event.preventDefault();
                                        }
                                      ">Hoàn thành</a>
                                     @elseif($project->status=='completed')
                                      <a href="" class="btn btn-warning" onclick="
                                        if(confirm('Bạn có muốn xóa ?')){
                                          event.preventDefault();
                                          document.getElementById('make_pending-{{$project->id}}').submit();
                                        }else{
                                          event.preventDefault();
                                        }
                                    ">Chờ xử lý</a>
                                     @endif
                                    <a href="{{route('project.edit',$project->id)}}" class="btn btn-info">Sửa</a>
                                    <a href="" class="btn btn-danger" onclick="
                                       if(confirm('Bạn có muốn xóa ?')){
                                         event.preventDefault();
                                         document.getElementById('delete-{{$project->id}}').submit();
                                       }else{
                                         event.preventDefault();
                                       }
                                    ">Xóa</a>
                                    <form id="delete-{{$project->id}}" style="display:none;" action="{{route('project.delete',$project->id)}}" method="POST">
                                      @csrf
                                      @method('delete')
                                    </form>
                                    <form id="make_completed-{{$project->id}}" style="display:none;" action="{{route('project.make_completed',$project->id)}}" method="POST">
                                      @csrf
                                    </form>
                                      <form id="make_pending-{{$project->id}}" style="display:none;" action="{{route('project.make_pending',$project->id)}}" method="POST">
                                        @csrf
                                      </form>
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    @endsection
